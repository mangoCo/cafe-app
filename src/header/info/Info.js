import React, { useState } from 'react';
import "./Info.css";

function Info() {
    const [flag, setFlag] = useState(window.outerWidth > 1200);

    function open(e) {
        e.preventDefault();
        setFlag(!flag);
    }
    return (
        <div className="info-box">
            <button onClick={open} className="btn-icon">
                <img className="icon-dots" src="../../icons/three_dots.svg" alt="dots" />
            </button>
            {flag &&
                <div className="address">
                    <p>г. Энгельс, ул. 148 Черниговской дивизии, 25</p>
                    <a href="tel:+79053248816" className="tel">+79053248816</a>
                </div>
            }
        </div>
    )
}

export default Info;