import React, { useState } from 'react';
import "./Menu.css";

function Menu() {
    const [flagMenu, setflagMenu] = useState(false);
    const [items, setItem] = useState(['О нас', 'Ассортимент', 'Популярное', 'Как заказать', 'Контакты']);

    function openMenu(e) {
        e.preventDefault();
        setflagMenu(!flagMenu);
    }
    return (
        <div className="menu-box">
            <button onClick={openMenu} className="btn-icon">
                {
                    flagMenu?  <img className="icon-arrow" src="../../icons/arrow.svg" alt="arrow"/> : <img className="icon-bar" src="../../icons/bars.svg" alt="bar" />
                }
                
            </button>
            <div className={flagMenu? 'list' : 'list list-hidden'}>
                <ul className='list-items'>
                    {items.map(item => <li key={item} className='item'><a>{item}</a></li>)}
                </ul>
            </div>
        </div>
    )
}

export default Menu;