import React from "react";
import "./Title.css";

function Title() {
    return <h1 className="title">Patisserie</h1>
}

export default Title;