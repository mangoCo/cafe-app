import React from "react";
import Title from "./title/Title";
import Menu from "./menu/Menu";
import Info from "./info/Info";
import "./Header.css";

function Header() {
    return (
        <div className="header">
            <Menu />
            <Title />
            <Info />
        </div>
    )
}

export default Header;