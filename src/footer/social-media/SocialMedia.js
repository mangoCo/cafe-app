import React from "react";
import './SocialMedia.css';

function SocialMedia() {
    return (
        <div className="social-media">
            <a href="https://www.instagram.com/loresch_elsa/" className="link-social"><img className="icon icon-insta" src="../../icons/insta.svg" /></a>
            <a href="https://www.youtube.com/@MarikoKitchen/playlists" className="link-social"><img className="icon icon-youtube" src="../../icons/youtube_icon.svg" /></a>
        </div>
    )
}

export default SocialMedia;