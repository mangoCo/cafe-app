import React from "react";
import './Contacts.css';

function Contacts() {
    return (
        <div className="contacts">
            <a href="tel:+79053248816" className="tel">+79053248816</a>
            <p className="our-address">413121, Саратовская область, <br />г. Энгельс, ул. 148 Черниговской дивизии, 25</p>
            <p>Ср - Пт: 12:00 - 19:00</p>
            <p>Сб - Вскр: 10:00 - 19:00</p>
            <p>Пн - Вт: выходной</p>
        </div>
    )
}

export default Contacts;