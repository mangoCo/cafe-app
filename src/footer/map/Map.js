import React, { useState, useCallback } from 'react';
import './Map.css';
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';

const containerStyle = {
    width: '100%',
    height: '360px'
}
function Map() {
    const [coordinates, setCoordinates] = useState({ lat: 51.477436348863336, lng: 46.143249607193646 });
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyCh23hMakE1ZZ9Ug2etYBO2nxBdlLJjd08"
    });

    return (
        <div className="map">
            {
                isLoaded ? (
                    <GoogleMap
                        mapContainerStyle={containerStyle}
                        center={coordinates}
                        zoom={17}
                    >
                        <Marker
                            position={coordinates}
                            icon={'http://localhost:3000/icons/location.png'}
                        />
                    </GoogleMap>
                ) : <></>
            }
        </div>
    )
}

export default Map;