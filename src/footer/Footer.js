import React from "react";
import "./Footer.css";
import SocialMedia from "./social-media/SocialMedia";
import Contacts from "./contacts/Contacts";
import Map from "./map/Map";

function Footer() {
    return (
        <div className="footer">
            <Map />
            <Contacts />
            <SocialMedia />
        </div>
    )
}

export default Footer;