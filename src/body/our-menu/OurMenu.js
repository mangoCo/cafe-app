import React from "react";
import "./OurMenu.css";

function OurMenu() {
    return (
        <div>
            <h2 className="our-menu">Меню</h2>
            <table>
                <tbody>
                    <tr>
                        <td>Лимонад</td>
                        <td>2400 руб</td>
                    </tr>
                    <tr>
                        <td>Морковный с манго</td>
                        <td>2600 руб</td>
                    </tr>
                    <tr>
                        <td>Мохито</td>
                        <td>2500 руб</td>
                    </tr>
                    <tr>
                        <td>Баноффи пай</td>
                        <td>2300 руб</td>
                    </tr>
                    <tr>
                        <td>Йогуртовый</td>
                        <td>2500 руб</td>
                    </tr>
                    <tr>
                        <td>Яблочный глинтвейн</td>
                        <td>2300 руб</td>
                    </tr>
                    <tr>
                        <td>Кофемания</td>
                        <td>2500 руб</td>
                    </tr>
                    <tr>
                        <td>Тропический</td>
                        <td>2600 руб</td>
                    </tr>
                    <tr>
                        <td>Медовый</td>
                        <td>2300 руб</td>
                    </tr>
                </tbody>
            </table>
            <div className="show-more">
                <a className="link">Показать больше <img className="icon icon-rotate" src="../../icons/arrow.svg" alt="arrow"/></a>
            </div>
        </div>
    )
}

export default OurMenu;