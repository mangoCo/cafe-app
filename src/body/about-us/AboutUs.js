import React from "react";
import './AboutUs.css';

function AboutUs(){
    return (
        <div className="about-us">
            <h2>Десерты - это наше ремесло и наша страсть.</h2>
            <div>Наша миссия поделиться с вами десертами, приготовленными из натуральных и качественных продуктов. Мы знаем, что у наших гостей есть выбор, и хотим, чтобы они чувствовали себя особенными.</div>
        </div>
    )
}

export default AboutUs;