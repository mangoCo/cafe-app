import React, { useState } from "react";
import './Popular.css';

function Popular() {
    const [popularProduct, setPopularProduct] = useState([{ photo: "../../images/1.png", name: 'Тропический', price: '2600 руб' }, { photo: "../../images/7.png", name: 'Яблочный глинтвейн', price: '2600 руб' }, { photo: "../../images/3.png", name: 'Мохито', price: '2600 руб' }, { photo: "../../images/8.png", name: 'Медовый', price: '2600 руб' }]);
    return (
        <div className="popular">
            <h2>Популярное</h2>
            {
                popularProduct.map(item => <div key={item.name} className="products">
                    <div className="products-logo">
                        <img className="products-img" src={item.photo} />
                        <div className="overlay">
                            <img className="icon-bag" src="../../icons/bag_shopping.svg"/>
                        </div>
                    </div>
                    <h4>{item.name}</h4>
                    <div>{item.price}</div>
                </div>)
            }

        </div>
    )
}

export default Popular;