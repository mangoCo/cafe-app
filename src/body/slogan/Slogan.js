import React from "react";
import "./Slogan.css";

function Slogan() {
    return (
        <div className="slogan-box">
            <h2 className="slogan">Любовь с первого кусь.</h2>
            <div className="text">Насладитесь лучшими десертами в кофейне Patisserie.</div>
            <div>
                <img className="img-cake" src="../../images/2.png"/>
                <img className="img-cake" src="../../images/4.png"/>
                <img className="img-cake" src="../../images/3.png"/>
            </div>
        </div>

    )
}

export default Slogan;