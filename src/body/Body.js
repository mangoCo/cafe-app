import React from "react";
import "./Body.css";
import Slogan from "./slogan/Slogan";
import AboutUs from "./about-us/AboutUs";
import OurMenu from "./our-menu/OurMenu";
import Popular from "./popular/Popular";

function Body() {
    return (
        <div className="body">
            <Slogan />
            <AboutUs />
            <OurMenu />
            <hr className="separate"/>
            <Popular />
        </div>
    )
}

export default Body;